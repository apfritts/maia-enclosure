$fn = 360;

module star() {
    x = 36.0;
    y = 2 * x;
    
    beam = 5.0;
    arm = 5.0;
    depth = 5.0;

    point1 = [0, 0];
    joint12 = [sin(x/2)*arm, cos(x/2)*arm];
	echo(joint12);
    point2 = joint12 + [sin(y)*arm, -0.5];
	echo(point2);
    joint23 = [5, 8];
	echo(joint23);
	#translate(joint23) circle(1);
    point3 = [arm * 2, arm * 2];
    point4 = [-arm * 2, arm * 2];
    point5 = [-arm * 2, arm];

    joint34 = [0, 8];
    joint45 = [-5, 8];
    joint51 = [-joint12[0], joint12[1]];

    polygon([point1, joint12, point2, joint23, point3, joint34, point4, joint45, point5, joint51]);
}

star();
