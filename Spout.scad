
module Spout(outD, inD, wallThickness, protrusion) {
    rotate([0, 45, 0])
        difference() {
            cylinder(h=20, d=10);
            translate([0, 0, -1])
                cylinder(h=22, d=8);
        }
}

module SpoutHole(inD, wallThickness) {
    
    translate([0, 0, inD/2])
        rotate([0, 90, 0])
        cylinder(d=inD, h=wallThickness+2);
}
