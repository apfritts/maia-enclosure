/*
    Mounts for Raspberry Pi +. These mounts include 1mm of clearance to
    intersect the platform they are mounted on.
*/

module PiPlusMount(x_offset=0, y_offset=0) {
    width=58;
    depth=48.5;
    xo=2.5 + x_offset;
    yo=2.5 + y_offset;

    translate([xo, yo, 0])
        Post(5, 4, 1);
    translate([xo, yo+depth, 0])
        Post(5, 4, 1);
    translate([xo+width, yo, 0])
        Post(5, 4, 1);
    translate([xo+width, yo+depth, 0])
        Post(5, 4, 1);
}

module Post(diameter, height, screwDiameter) {
    difference() {
        cylinder(d=diameter, h=height);
        translate([0, 0, 1])
            cylinder(d1=screwDiameter, d2=screwDiameter-0.1, h=height);
    }
}

module AmplifierMount() {
    longDistance=38;
    shortDistance=34;
    Post(5, 2.5, 1);
    translate([longDistance, 0, 0])
        Post(5, 2.5, 1);
    translate([0, shortDistance, 0])
        Post(5, 2.5, 1);
    translate([longDistance, shortDistance, 0])
        Post(5, 2.5, 1);
}
