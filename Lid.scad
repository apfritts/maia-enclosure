include <Wall.scad>
include <PiPlusMount.scad>

width=200;
depth=200;
wall_thickness=2.5;

piPlusX=52/2;
piPlusY=58/2;

hull() {
    translate([0, 0, wall_thickness])
        Wall(depth-(wall_thickness*2), 0.1, width-(wall_thickness*2), wall_thickness);
    Wall(depth, 0.1, width, wall_thickness);
}
translate([piPlusX, -piPlusY, wall_thickness-1])
    rotate(90)
    PiPlusMount();
