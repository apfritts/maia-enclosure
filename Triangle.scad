module Triangle(depth, width, height) {
    points = [
    [     0,      0,       0 ],  //0
    [ depth,      0,       0 ],  //1
    [ depth,  width,       0 ],  //2
    [     0,  width,       0 ],  //3
    [     0,      0,  height ],  //4
    [ depth,      0,  height ]]; //5

    faces = [
    [0,1,2,3],  // bottom
    [1,2,5],  // front
    [5,4,3,2],  // top
    [4,3,0],  // back
    [5,4,0,1]]; // left

    polyhedron(points, faces);
}
