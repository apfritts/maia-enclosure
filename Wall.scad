/*
    This draws a simple box with rounded side edges and a flat top.
    The height, width, and depth are absolute and the radius does not change 
    the actual dimensions of the box.
*/

module Wall(width, height, depth, radius, fn=60) {
    c2c_width=width-(radius*2);
    c2c_depth=depth-(radius*2);
    hull() {
        translate([-c2c_width/2, -c2c_depth/2, 0])
            cylinder(r=radius,h=height, $fn=fn);
        translate([c2c_width/2, -c2c_depth/2, 0])
            cylinder(r=radius,h=height, $fn=fn);
        translate([-c2c_width/2, c2c_depth/2, 0])
            cylinder(r=radius,h=height, $fn=fn);
        translate([c2c_width/2, c2c_depth/2, 0])
            cylinder(r=radius,h=height, $fn=fn);
    }
}
