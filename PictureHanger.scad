include <PictureHangerHook.scad>
include <Star2.scad>

module star() {
	hull() {
		intersection() {
			translate([0, -4, 0])
				circle(5);
			ShittyStar();
		}
	}
	ShittyStar();
}

module HallowStar() {
    difference() {
        star();
        translate([0, -0.6, 0])
            scale([0.8, 0.8, 1])
            star();
    }
}

module TheStar() {
    HallowStar();
    rotate([0, 60, 0])
        HallowStar();
    rotate([0, -60, 0])
        HallowStar();
}

difference() {
    TheStar();
    translate([-15, -15, -15.5])
        cube([30, 30, 15]);
}
translate([3, 0, 0.5])
    rotate([180, 90, 0])
    PictureHangerHook(topWidth=6, bottomWidth=3, underhang=7);
