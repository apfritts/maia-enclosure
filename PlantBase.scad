include <Mug.scad>
include <Spout.scad>

// Measurements in mm

width=70;
height=20;
wallThickness=5;
//$fn=300;


Mug(200, 30, wallThickness);
difference() {
    Mug(150, 250, wallThickness);
    translate([width-2, 0, wallThickness])
        SpoutHole(10, wallThickness+1);
}

