/*
    Mounts for Raspberry Pi +. These mounts include 1mm of clearance to
    intersect the platform they are mounted on.
*/

module MouseEar(diameter=25.4) {
    cylinder(h=1, d=diameter, center=false);
}


