module Mug(width, height, wallThickness=5)
{
    r_of_inside=width/2-wallThickness;
    difference()
    {
        translate([0,0,height/2])
        intersection()
        {
            cube([width,width,height], center=true);
            scale([1,1,height/width])
            sphere(width/2 * sqrt(2));
        }
        translate([0,0,wallThickness])
            cylinder(r=r_of_inside,h=height+0.1);
    }
}
