include <MouseEar.scad>
include <PiPlusMount.scad>
include <Wall.scad>

width=200;
depth=200;
height=70;
wall_thickness=2.5;


// Speaker dimensions
inch=25.4;
speakerDiameter=130;
screw2screw = 200.818;
speakerScrewPost=(6/16)*inch;
speakerDepth=41.5;

union() {
    difference() {
        difference() {
            translate([depth/2, width/2, 0]) {
                difference() {
                    Wall(depth, height, width, wall_thickness);
                    translate([0, 0, wall_thickness])
                        Wall(depth-(wall_thickness*2), height-wall_thickness+1, width-(wall_thickness*2), wall_thickness);
                }
            }
            rotate([0, 180, 0])
                translate([-depth/2, width/2, -height-0.1]) {
                    import("Lid.stl");
            }
        }
        translate([wall_thickness + (speakerDiameter/2) + 10, width/2, -1])
            cylinder(d=131, h=wall_thickness+2);
    }
}

mountDepth = depth - wall_thickness + 0.05;
heightFromBottom = wall_thickness + 15;
distanceFromRight = width - 30;


translate([mountDepth, distanceFromRight, heightFromBottom])
    rotate([90, 0, -90])
    AmplifierMount();


translate([mountDepth, 90, wall_thickness + 5])
    rotate([90, 0, -90])
    PiPlusMount();
