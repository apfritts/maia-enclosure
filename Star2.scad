
module tip() {
	point1 = [-(cos(72) * 10), 0, 0];
	point2 = [0, (cos(18) * 10), 0];
	point3 = [cos(72) * 10, 0, 0];
	polygon([point1, point2, point3, point1]);
}

module ShittyStar() {
    tip();
    translate([-(cos(72) * 10), -(cos(72) * 10), 0])
        rotate([0, 0, 72])
        tip();
    translate([-(cos(72) * 10), -(cos(72) * 10)*2, 0])
        rotate([0, 0, 72*2])
        tip();
    translate([(cos(72) * 10), -(cos(72) * 10) * 2, 0])
        rotate([0, 0, 72*3])
        tip();
    translate([(cos(72) * 10), -(cos(72) * 10), 0])
        rotate([0, 0, 72*4])
        tip();
}
