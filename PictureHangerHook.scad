module PictureHangerHook(topWidth=10, bottomWidth=5, underhang=7) {
    overhang=3;
    lip=2;
    thickness = 1;

    cube([overhang, thickness, topWidth]);
    translate([overhang-thickness, 0, 0])
        cube([thickness, lip, topWidth]);
    hull() {
        cube([thickness, thickness, topWidth]);
        translate([0, underhang, (topWidth-bottomWidth)/2])
            cube([thickness, thickness, bottomWidth]);
    }
    translate([-lip, underhang, (topWidth-bottomWidth)/2])
        cube([lip, thickness, bottomWidth]);
    translate([-lip, underhang-thickness, (topWidth-bottomWidth)/2])
        cube([thickness, thickness, bottomWidth]);
}
