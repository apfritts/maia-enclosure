module SimCardTool() {
	headWidth=14;
	pinLength=8;
	toolDepth=0.4;

	translate([headWidth/2,headWidth/2,0]) {
		cylinder(h=toolDepth, d=headWidth);
		translate([headWidth/2-0.1,0,0])
            cube([pinLength, toolDepth, toolDepth]);
	}
}