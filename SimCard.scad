
module SimCard() {
	height = 12.3;
	width = 8.8;
	depth = 0.67;
	
	difference() {
	    cube([width, height, depth], 0);
	    translate([width+1, -9, -1])
	        rotate([0, 0, 45])
	            cube(10, 0);
	}
}
