include <SimCard.scad>;
include <SimCardTool.scad>;

$fn = 360;

bufferToEdge = 1;
width = 8.8 * 4 + bufferToEdge * 5;
height = 12.3 + bufferToEdge * 2 + 2;
depth = 0.67 + bufferToEdge;

difference() {
	hull() {
		translate([0, 0.5, 0.5])
			cube([width, height-1, depth-1]);
		translate([0.5, 0, 0.5])
			cube([width-1, height, depth-1]);
		translate([0.5, 0.5, 0])
			cube([width-1, height-1, depth]);
	}
	translate([bufferToEdge, bufferToEdge + 1, bufferToEdge])
		hull() {
			SimCard();
			translate([0,0,0.5])
				SimCard();
		};
	translate([8.8 * 1 + bufferToEdge * 2, bufferToEdge + 1, bufferToEdge])
		hull() {
			SimCard();
			translate([0,0,0.5])
				SimCard();
		};
	translate([8.8 * 2 + bufferToEdge * 3 + 4, bufferToEdge-2.5, bufferToEdge]) {
		rotate([0, 0, 30])
			SimCardTool();
		translate([0,0,0.3])
			rotate([0, 0, 30])
				SimCardTool();
	}
    // Rotate SimCardTool into holder so that you can push down on one side of it
    // to get it out. Probably same for SIM cards
}

translate([(width + 37) / 2, 7, 0])
	mirror()
	color("red")
    linear_extrude(0.1) {
        text("travel safely and fast", 3);
    }