include <PiPlusMount.scad>

width=58;
height=1;
depth=48.5;
xo=2.5;
yo=2.5;
$fn=60;

difference() {
    hull() {
        translate([xo, yo, 0])
            Post(5, height, 1);
        translate([xo, yo+depth, 0])
            Post(5, height, 1);
    }
    translate([2.5, 2.5, -1])
        cylinder(d=1, h=3);
    translate([2.5, depth+2.5, -1])
        cylinder(d=1, h=3);
}
difference() {
    hull() {
        translate([xo+width, yo, 0])
            Post(5, height, 1);
        translate([xo+width, yo+depth, 0])
            Post(5, height, 1);
    }
    translate([2.5+width, 2.5, -1])
        cylinder(d=1, h=3);
    translate([2.5+width, depth+2.5, -1])
        cylinder(d=1, h=3);
}

translate([xo, depth/2, 0])
    cube([width, 5, 1]);

cameraMountWidth=25;

translate([1, (depth-cameraMountWidth)/2+yo, 0]) {
    translate([0, 1, 0])
        cube([3, 1, 21]);
    translate([1, 0, 0])
        cube([1, cameraMountWidth, 21]);
    translate([0, 23, 0])
        cube([3, 1, 21]);

    translate([2, 0, 20])
    rotate([0, -90, 0]) {
        difference() {
            cube([21, 3, 1]);
            translate([19.5, 1.5, -1])
                cylinder(d=1, h=3);
            translate([19.5-12, 1.5, -1])
                cylinder(d=1, h=3);
        }
        translate([0, 2, 0])
            cube([3, 21, 1]);
        difference() {
            translate([0, 22, 0])
                cube([21, 3, 1]);
            translate([19.5, 23.5, -1])
                cylinder(d=1, h=3);
            translate([19.5-12, 23.5, -1])
                cylinder(d=1, h=3);
        }
    }
}

//translate([0, 70, 0])
//    PiPlusMount(height=20);


