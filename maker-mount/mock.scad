underHangDepth=43;
topDepth=49;
hangHeight=7;
topWidth=10;
frontHanger=15;

// Underhang
cube([underHangDepth, topWidth, 1]);

// Connector
translate([0, 0, frontHanger-hangHeight-2])
    cube([1, topWidth, hangHeight+2]);

// Top arm
translate([0, 0, frontHanger-1])
    cube([topDepth+1, topWidth, 1]);

// Front gripper
translate([topDepth, 0, 0])
    rotate([90, 0, 90])
    cube([topWidth, frontHanger, 1]);
