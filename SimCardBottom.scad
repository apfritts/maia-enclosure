include <SimCard.scad>;

bufferToEdge = 2;
width = 8.8 * 4 + bufferToEdge * 5;
height = 12.3 + bufferToEdge * 2;
depth = 0.67 + bufferToEdge;

difference() {
	hull() {
		translate([0, 0.5, 0.5])
			cube([width, height-1, depth-1]);
		translate([0.5, 0, 0.5])
			cube([width-1, height, depth-1]);
		translate([0.5, 0.5, 0])
			cube([width-1, height-1, depth]);
	}
	translate([bufferToEdge, bufferToEdge, bufferToEdge])
		hull() {
			SimCard();
			translate([0,0,0.5])
				SimCard();
		};
	translate([8.8 * 1 + bufferToEdge * 2, bufferToEdge, bufferToEdge])
		hull() {
			SimCard();
			translate([0,0,0.5])
				SimCard();
		};
	translate([8.8 * 2 + bufferToEdge * 3, bufferToEdge, bufferToEdge])
		hull() {
			SimCard();
			translate([0,0,0.5])
				SimCard();
		};
	translate([8.8 * 3 + bufferToEdge * 4, bufferToEdge, bufferToEdge])
		hull() {
			SimCard();
			translate([0,0,0.5])
				SimCard();
		};
}